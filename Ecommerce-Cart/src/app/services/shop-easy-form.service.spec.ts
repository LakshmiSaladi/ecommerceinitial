import { TestBed } from '@angular/core/testing';

import { ShopEasyFormService } from './shop-easy-form.service';

describe('ShopEasyFormService', () => {
  let service: ShopEasyFormService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(ShopEasyFormService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
